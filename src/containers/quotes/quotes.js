import React, {useEffect, useState} from 'react';
import './quotes.css';
import axiosQuote from "../../axiosQuote";
import withErrorHandler from "../../hoc/ErrorHandler";

const Quotes = props => {
    const [newQuotes, setNewQuotes] = useState([]);
    const [quote, setQuote] = useState([{response:'/quotes.json'}]);
    const quoteCopy = [...quote];
    const [name, setName] = useState('All');

    useEffect(() => {
        const fetchData = async () => {
            const quotes = await axiosQuote.get(quote[0].response);
            const newQuotesCopy = [...newQuotes];
            newQuotesCopy.length = 0;
            for (const key in quotes.data) {
                newQuotesCopy.push({
                    quote: quotes.data[key].quote,
                    author: quotes.data[key].author,
                    id: key,
                    category: quotes.data[key].category,
                    display: 'block',
                });
            }
            setNewQuotes(newQuotesCopy);
        };
        fetchData().catch(e => console.log(e));
    }, [quote]);

    const deletePost = async event => {
        const newQuotesCopy = [...newQuotes];
        newQuotesCopy[event.target.id].display = 'none';
        setNewQuotes(newQuotesCopy);
        await axiosQuote.delete('/quotes/' + event.target.value + '.json');
    };

    const editPost = event => {
        const params = new URLSearchParams({key: event.target.value});
        props.history.push({
            pathname: '/edit',
            search: '?' + params.toString(),
        });
    }

    const quoteList = newQuotes.map((quote, index) => {
        return (
            <div className="quote" key={quote.id} style={{display: quote.display}}>
                <button type="button" value={quote.id} className="delete" id={index} onClick={deletePost}>X</button>
                <p className="quoteTxt">"{quote.quote}"</p>
                <p className="author">© {quote.author}</p>
                <button type="button" value={quote.id} className="edit" onClick={editPost}>Edit</button>
            </div>
        );
    });

    const getCategory = event => {
        if (event.target.innerHTML === 'All') {
            quoteCopy[0] = {response:'/quotes.json'};
            setQuote(quoteCopy);
        } else {
            quoteCopy[0] = {response:'/quotes.json?orderBy="category"&equalTo="'+ event.target.innerHTML +'"'};
            setQuote(quoteCopy);
        }
        setName(event.target.innerHTML);
    };

        return (
            <div className="container">
                <div className="quotesBlock">
                    <div className="list">
                        <ul>
                            <li onClick={getCategory} className="link">All</li>
                            <li onClick={getCategory} className="link">Movies</li>
                            <li onClick={getCategory} className="link">Cartoons</li>
                            <li onClick={getCategory} className="link">Famous People</li>
                            <li onClick={getCategory} className="link">Motivational</li>
                            <li onClick={getCategory} className="link">Beautiful</li>
                        </ul>
                    </div>
                    <div className="quotes">
                        <h2 style={{margin: 0}}>{name}</h2>
                        {quoteList}
                    </div>
                </div>
            </div>
        )
};

export default withErrorHandler(Quotes, axiosQuote);