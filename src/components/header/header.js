import React from 'react';
import './header.css';

const Header = props => {
    const quotesChanger = () => {
        props.history.push("/");
    };

    const quotesSubmitChanger = () => {
        props.history.push("/submitQuote");
    };

    return (
        <div className="container">
            <header className="header">
                <h3 className="headerHeading">Quotes Central</h3>
                <div className="links">
                    <button type="button" onClick={quotesChanger} className="link" style={{borderRight: '2px solid black'}}>Quotes</button>
                    <button type="button" onClick={quotesSubmitChanger} className="link">Submit new quote</button>
                </div>
            </header>
        </div>
    );
};

export default Header;