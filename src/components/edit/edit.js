import React, {useState, useEffect} from 'react';
import './edit.css';
import axiosQuote from "../../axiosQuote";
import Spinner from "../spinner/spinner";

const Edit = props => {
    const [load, setLoad] = useState(false)
    const [editQuote, setEditQuote] = useState([{
        quote: '', author: '', category: '',
    }]);
    const editQuoteCopy = [...editQuote];

    const [categories] = useState([
        {name: "Movies", selected: "false"}, {name: "Cartoons", selected: "false"},
        {name: "Famous People", selected: "false"},
        {name: "Motivational", selected: "false"}, {name: "Beautiful", selected: "false"},
        ]);

    const getParams = () => {
        const params = new URLSearchParams(props.location.search);
        return Object.fromEntries(params);
    };
    const [keys] = useState(getParams);

    useEffect(() => {
        const fetchData = async () => {
            setLoad(true)
            const quoteInner = await axiosQuote.get('/quotes/'+ keys.key +'.json');
            for (const key in quoteInner.data) {
                editQuoteCopy[0] = {
                    quote: quoteInner.data.quote,
                    author: quoteInner.data.author,
                    category: quoteInner.data.category,
                }
            }
            setEditQuote(editQuoteCopy);
            setLoad(false)
        };
        fetchData().catch(e => console.log(e));
    }, []);

    const quoteDataChanger = event => {
        editQuoteCopy[0].quote = event.target.value;
        setEditQuote(editQuoteCopy);
    };

    const authorDataChanger = event => {
        editQuoteCopy[0].author = event.target.value;
        setEditQuote(editQuoteCopy);
    };

    const categoryDataChanger = event => {
        editQuoteCopy[0].category = event.target.value;
        setEditQuote(editQuoteCopy);
    };

    const categoriesCopy = [...categories];

    for (const key in categories) {
        if (categories[key].name === editQuote[0].category) {
            categoriesCopy[key].selected = true;
        }
        else {
            const categoriesCopy = [...categories];
            categoriesCopy[key].selected = false;
        }
    }

    const categoriesList = categoriesCopy.map((name, indexed) => {
        return(
            <option value={name.name} selected={name.selected} key={name.name}>{name.name}</option>
        );
    });

    const sendEditedQuote = async event => {
        event.preventDefault()
        try {
            await axiosQuote.put('/quotes/'+ keys.key +'.json', {...editQuote[0]});
        } finally {
            props.history.push('/');
        }
    };

    if (load) {
        return (<Spinner/>);
    } else {
        return (
            <div className="container">
                <form className="editQuotes" onSubmit={sendEditedQuote}>
                    <h2>Edit Quotes</h2>
                    <label htmlFor="editCategory">Choose category: </label>
                    <select name="editCategory" className="categoryChanger" onChange={categoryDataChanger}>
                        {categoriesList}
                    </select>
                    <input
                        type="text" value={editQuote[0].author}
                        className="areaForAuthorEdit" onChange={authorDataChanger}
                        name="author" placeholder="Author"
                    />
                    <textarea
                        value={editQuote[0].quote}
                        onChange={quoteDataChanger} name="quote"
                        className="areaForQuoteEdit" placeholder="Quote"
                    />
                    <button type="submit" className="editBtn">Save</button>
                </form>
            </div>
        );
    }
};

export default Edit;