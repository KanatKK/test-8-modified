import React, {useEffect, useMemo, useState} from "react";
import Spinner from "../components/spinner/spinner";

const withErrorHandler = (WrappedComponent, axios) => {
    return function WithErrorHandler(props) {
        const [loading, setLoading] = useState([false]);
        const loadingCopy = [...loading]

        const req = useMemo(() => {
            axios.interceptors.request.use(request => {
                loadingCopy[0] = true
                console.log(loadingCopy[0])
                return(request);
            });
        }, []);

        useEffect(() => {
            return () => axios.interceptors.request.eject(req);
        },[req]);

        const res = useMemo(() => {
            axios.interceptors.response.use(response => {
                loadingCopy[0] = false
                console.log(loadingCopy[0])
                return(response);
            });
        }, []);

        useEffect(() => {
            return () => axios.interceptors.response.eject(res);
        },[res]);

        if (loadingCopy[0] === true) {
            return <Spinner/>
        } else {
            return <WrappedComponent {...props}/>
        }
    }
}

export default withErrorHandler;